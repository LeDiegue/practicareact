import React from 'react';
import logo from './../../logo.svg';
import './Titulo.css'

export default function Titulo(props) {

    return (
        <div>
            <h4>{props.text}</h4>
            <button onClick={props.cambiarTexto}>Cambiar text</button>
            <button onClick={props.mostrarHola}>Saludar</button>
        </div>
    )
}

