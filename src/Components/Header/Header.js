import React, { Component } from 'react';
import logo from './../../logo.svg';
import Titulo from './../Titulo/Titulo';
import './Header.css'

export default class Header extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: 'hola',
            nombre: '',
            edad: 2
        };
    }

    saludar(){
        alert('hola')
    }

    render() {
        return (
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                    Edit <code>src/App.js</code> and save to reload.
            </p>
                {this.props.currentUser.nombre? (
                    <p>Usuario: {this.props.currentUser.nombre}</p>
                ):(
                    <p>No hay usuario</p>
                )}
                
                <Titulo text={this.state.text} mostrarHola={this.saludar} cambiarTexto={() => this.setState({ text: 'elevando estado' })} />
                <button onClick={() => this.setState({ text: 'ja' })}>Cambiar</button>
            </header>
        )
    }
}

const hola = new Header();
