import React, { useState } from 'react';
import './App.css';
import Header from './Components/Header/Header'
import User from './Components/User/User'

function App() {
  const [currentUser, setCurrentUser] = useState({});

  return (
    <div className="App">
      <Header currentUser={currentUser} />
      <User setCurrentUser={(name) => setCurrentUser({ nombre: name})} />
    </div>
  );
}

export default App;
